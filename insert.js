var Client = require('coinstack-sdk-js');


var apiKey = "eb90dbf0-e98c-11e4-b571-0800200c9a66"
var secretKey = "f8bd5b50-e98c-11e4-b571-0800200c9a66"
//var client = new Client(apiKey, secretKey, "52.78.93.213:8080", "http")
var client = new Client(apiKey, secretKey, "127.0.0.1:3000", "http")
//var client = new Client(apiKey, secretKey, "testchain.blocko.io", "http")


var txCount = 0;
var failCount = 0;
var startTime = new Date().getTime();
// L3tmvQDHgrkaNsLGQL9tREpLReHGWtBngDg6M7Pis5Mx8qSce8Qj
// 17YnnND81ZWip7md2F82FKBLymMYd7LZzh
function sendTx() {
    var txBuilder = client.createTransactionBuilder();
    // var base = new Buffer("");
    // var data = new Buffer(1000000);
    var data = new Buffer(80);
    data.fill(0);
    // for (var i = 0; i < 100000; i++) {
    //     data = Buffer.concat([data, base])
    // }
    console.log("data size:", data.length);
    //txBuilder.setDataOutput(data);
    txBuilder.setInput("1NdgqKo6MgyspmwnXmwNTFLCqmLdpoFowB");
    for (var i = 0; i < 1; i++) {
        txBuilder.addOutput("1Dvgabdn4iiGnFijUoCCtWGoE69SN1qeUZ", Client.Math.toSatoshi(0.00001));
    }


    txBuilder.setFee(Client.Math.toSatoshi(0.0001));
    txBuilder.buildTransaction(function (err, tx) {
        if (err) {
            console.log(err, err.stack);
            return;
        }
        tx.sign("L1w5UHhbPfbUumGomryB88fA3ENZ5cNWuJmDxMNn9YSp7A4nJgJ8");
        var rawTx = tx.serialize();
        var txSize = rawTx.length / 2;
        console.log("[+" + txCount + "][-" + failCount + "]sending tx (" + tx.getHash() + ") - ", txSize, "bytes");
        // console.log(rawTx);


        client.sendTransaction(rawTx, function (err) {
            var elapsed = (new Date().getTime() - startTime) / 1000;
            var rate = txCount / elapsed;
            console.log("[" + txCount + "] sent tx (" + tx.getHash() + ") - ", txSize, "bytes (", rate, "txps)");
            if (err) {
              failCount++;
              console.log("failed to send tx (" + tx.getHash() + ")");
console.log(rawTx);
            } else {
              txCount++;
		    console.log("sent tx (" + tx.getHash() + ")");
            }
            // if (txCount < 0) {
            //     setTimeout(function () {
                      sendTx();
            //         txCount++;
            //     }, 100);
            // }
            //}
            return;
        });
    })
}

sendTx();
